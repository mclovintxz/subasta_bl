package baca.brian.bl.logic;
import baca.brian.bl.BL;
import baca.brian.bl.entities.Usuario.Administrador;
import baca.brian.bl.entities.Subasta.*;
import baca.brian.bl.entities.Usuario.*;

import java.time.LocalDate;

public class Gestor {
 static BL bl = new BL();

    public Gestor (){
    }

    public  void registrarAdministradorr(String nombre, String apellido, String id,String contrasena, int year,int mes,int dia, int edad,String estado,String email, int tipo){
        LocalDate fecha = LocalDate.of(year,mes,dia);
        Administrador admin = new Administrador(email,tipo,nombre,apellido,id,fecha,edad,estado,contrasena);
        bl.registrarAdministrador(admin);
    }
}
