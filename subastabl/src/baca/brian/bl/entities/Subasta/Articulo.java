package baca.brian.bl.entities.Subasta;
import java.time.LocalDate;

/**
 * @author Brian Alonso Baca
 */
public class Articulo
{
    private String nombreArticulo;
    private String descripcion;
    private String estado;
    private LocalDate fechaAntiguedad;
    private String categoria;
    private int valor;
    private String fechainicio;
    private int id;

    /**
     * Contrusctor por defecto
     */
    public Articulo (){

    }

    /**
     * Instantiates para Articulo
     *
     * @param nombreArticulo  el nombre del articulo
     * @param descripcion     la descripcion del articulo
     * @param estado          el estado del articulo
     * @param fechaAntiguedad la fecha antiguedad del articulo
     * @param categoria       la categoria del articulo
     * @param valor           el valor del articulo
     * @param fechainicio     la fechainicio del articulo
     */
    public Articulo(String nombreArticulo, String descripcion, String estado, LocalDate fechaAntiguedad, String categoria, int valor, String fechainicio,int id) {
        this.nombreArticulo = nombreArticulo;
        this.descripcion = descripcion;
        this.estado = estado;
        this.fechaAntiguedad = fechaAntiguedad;
        this.categoria = categoria;
        this.valor = valor;
        this.fechainicio = fechainicio;
        this.id = id;
    }

    /**
     * Gets de la  descripcion.
     *
     * @return la descripcion
     */
    public String getDescripcion()
    {
        return descripcion;
    }

    /**
     * Sets de la descripcion.
     *
     * @param descripcion la descripcion
     */
    public void setDescripcion(String descripcion)
    {
        this.descripcion = descripcion;
    }

    /**
     * Gets del  valor.
     *
     * @return el valor
     */
    public int getValor()
    {
        return valor;
    }

    /**
     * Sets del  valor.
     *
     * @param valor el valor
     */
    public void setValor(int valor)
    {
        this.valor = valor;
    }

    /**
     * Gets de la fechainicio.
     *
     * @return la fechainicio
     */
    public String getFechainicio()
    {
        return fechainicio;
    }

    /**
     * Sets de la fechainicio.
     *
     * @param fechainicio la fechainicio
     */
    public void setFechainicio(String fechainicio)
    {
        this.fechainicio = fechainicio;
    }

    /**
     * Gets del nombre articulo.
     *
     * @return el nombre articulo
     */
    public String getNombreArticulo() {
        return nombreArticulo;
    }

    /**
     * Sets para el nombre articulo.
     *
     * @param nombreArticulo el nombre articulo
     */
    public void setNombreArticulo(String nombreArticulo) {
        this.nombreArticulo = nombreArticulo;
    }

    /**
     * Gets del estado.
     *
     * @return el estado
     */
    public String getEstado() {
        return estado;
    }

    /**
     * Sets para estado.
     *
     * @param estado el estado
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     * Gets de la fecha antiguedad.
     *
     * @return la fecha antiguedad
     */
    public LocalDate getFechaAntiguedad() {
        return fechaAntiguedad;
    }

    /**
     * Sets para fecha antiguedad.
     *
     * @param fechaAntiguedad la fecha antiguedad
     */
    public void setFechaAntiguedad(LocalDate fechaAntiguedad) {
        this.fechaAntiguedad = fechaAntiguedad;
    }

    /**
     * Gets de la categoria.
     *
     * @return la categoria
     */
    public String getCategoria() {
        return categoria;
    }

    /**
     * Sets para categoria.
     *
     * @param categoria la categoria
     */
    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return
                "Nombre Articulo: " + nombreArticulo + '\n' +
                "Descripcion: " + descripcion + '\n' +
                "Estado: " + estado + '\n' +
                "Fecha de antiguedad del articulo: " + fechaAntiguedad + '\n' +
                "Categoria: " + categoria + '\n' +
                "Valor: " + valor + '\n' +
                "Fecha Inicio: " + fechainicio;
    }
}
