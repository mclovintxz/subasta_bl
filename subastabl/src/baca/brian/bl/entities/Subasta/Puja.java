package baca.brian.bl.entities.Subasta;

/**
 * @author Brian Alonso Baca
 */

public class Puja
{
    private String nombrecomprador;
    private int valorpuja;
    private int codigosubasta;
    private int idPuja;

    /**
     * Constructor por defecto
     */
    public Puja (){

    }

    /**
     * Instantiates para una Puja
     *
     * @param nombre the nombre
     * @param valor  the valor
     * @param codigo the codigo
     */
    public Puja(String nombre, int valor, int codigo, int idPuja)
    {
        this.nombrecomprador = nombre;
        this.valorpuja = valor;
        this.codigosubasta = codigo;
        this.idPuja = idPuja;
    }

    /**
     * Gets para el nombrecomprador.
     *
     * @return el nombrecomprador
     */
    public String getNombrecomprador()
    {
        return nombrecomprador;
    }

    /**
     * Sets para el nombrecomprador.
     *
     * @param nombrecomprador nombre nombrecomprador
     */
    public void setNombrecomprador(String nombrecomprador)
    {
        this.nombrecomprador = nombrecomprador;
    }

    /**
     * Gets del valorpuja.
     *
     * @return el valorpuja
     */
    public int getValorpuja()
    {
        return valorpuja;
    }

    /**
     * Sets para valorpuja.
     *
     * @param valorpuja el valorpuja
     */
    public void setValorpuja(int valorpuja)
    {
        this.valorpuja = valorpuja;
    }

    /**
     * Gets para el  codigosubasta.
     *
     * @return el codigosubasta
     */
    public int getCodigosubasta()
    {
        return codigosubasta;
    }

    /**
     * Sets para el codigosubasta.
     *
     * @param codigosubasta el codigosubasta
     */
    public void setCodigosubasta(int codigosubasta)
    {
        this.codigosubasta = codigosubasta;
    }

    public int getIdPuja() {
        return idPuja;
    }

    public void setIdPuja(int idPuja) {
        this.idPuja = idPuja;
    }

    @Override
    public String toString() {
        return "Puja{" +
                "nombrecomprador='" + nombrecomprador + '\'' +
                ", valorpuja=" + valorpuja +
                ", codigosubasta=" + codigosubasta +
                '}';
    }
}


