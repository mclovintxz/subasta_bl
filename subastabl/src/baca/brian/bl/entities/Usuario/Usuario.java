package baca.brian.bl.entities.Usuario;
import baca.brian.bl.entities.Subasta.*;
import java.util.ArrayList;

/**
 * @author Brian Alonso Baca
 */
public class Usuario
{
    /**
     * El nombre
     */
    protected String nombre;
    /**
     * El Email.
     */
    protected String email;
    /**
     * El Tipo De Usuario
     */
    protected int tipo;
    private ArrayList<Subasta> subastas;
    private ArrayList<Puja> pujas;


    //tipo 0-> vendedor
    //tipo 1 -> coleccionista
    //tipo 2 -> Administrador


    /**
     * Constructor por defecto
     */
    public Usuario (){
    }

    /**
     * Instantiates para Usuario
     *
     * @param email  the email
     * @param tipo   the tipo
     * @param nombre the nombre
     */
    public Usuario(String email, int tipo, String nombre) {
        this.nombre = nombre;
        this.email = email;
        this.tipo = tipo;
        this.subastas = new ArrayList<Subasta>();
        this.pujas = new ArrayList<Puja>();
    }


    /**
     * Gets nombre.
     *
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Sets nombre.
     *
     * @param nombre the nombre
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Gets email.
     *
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets email.
     *
     * @param email the email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Gets tipo.
     *
     * @return the tipo
     */
    public int getTipo() {
        return tipo;
    }

    /**
     * Sets tipo.
     *
     * @param tipo the tipo
     */
    public void setTipo(int tipo) {
        this.tipo = tipo;
    }

    /**
     * Add subasta.
     *
     * @param s the s
     */
    public void addSubasta(Subasta s)
    {
        subastas.add(s);
    }

    /**
     * Add puja.
     *
     * @param p the p
     */
    public void addPuja(Puja p)
    {
        pujas.add(p);
    }

    /**
     * Gets pujas.
     *
     * @return the pujas
     */
    public ArrayList<Puja> getPujas()
    {
        return pujas;
    }

    /**
     * Sets pujas.
     *
     * @param pujas the pujas
     */
    public void setPujas(ArrayList<Puja> pujas)
    {
        this.pujas = pujas;
    }

    /**
     * Gets subastas.
     *
     * @return the subastas
     */
    public ArrayList<Subasta> getSubastas()
    {
        return subastas;
    }

    /**
     * Sets subastas.
     *
     * @param subastas the subastas
     */
    public void setSubastas(ArrayList<Subasta> subastas)
    {
        this.subastas = subastas;
    }


    @Override
    public String toString() {
        return "Usuario{" +
                "nombre='" + nombre + '\'' +
                ", email='" + email + '\'' +
                ", tipo=" + tipo +
                '}';
    }
}
