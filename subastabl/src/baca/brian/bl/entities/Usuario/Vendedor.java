package baca.brian.bl.entities.Usuario;
import baca.brian.bl.entities.Subasta.*;

import java.util.ArrayList;

/**
 * @author Brian Alonso Baca
 */
public class Vendedor extends Usuario{
    private String direccion;
    private ArrayList<Subasta> subastas;
    private ArrayList<Puja> pujas;

    /**
     * Constructor por defecto
     */
    public Vendedor (){
    }

    /**
     * Instantiates para vendedor
     *
     * @param email     the email
     * @param tipo      the tipo
     * @param nombre    the nombre
     * @param direccion the direccion
     */
    public Vendedor(String email, int tipo, String nombre, String direccion) {
        super(email, tipo, nombre);
        this.nombre = nombre;
        this.direccion = direccion;
    }


    /**
     * Gets direccion.
     *
     * @return the direccion
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     * Sets direccion.
     *
     * @param direccion the direccion
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @Override
    public String toString() {
        return "Vendedor{" +
                "nombre='" + nombre + '\'' +
                ", email='" + email + '\'' +
                ", tipo=" + tipo +
                ", direccion='" + direccion + '\'' +
                ", subastas=" + subastas +
                ", pujas=" + pujas +
                '}';
    }
}
